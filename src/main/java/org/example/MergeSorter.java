package org.example;

import java.util.Arrays;

public class MergeSorter {


    public static <T extends Comparable<T>> void sort(T[] array){

        if (array.length < 2)
            return;

        int middle = array.length / 2;
        T[] leftArray = Arrays.copyOfRange(array,0, middle);
        T[] rightArray = Arrays.copyOfRange(array, middle, array.length);
        sort(leftArray);
        sort(rightArray);

        merge(array,leftArray,rightArray);
    }

    private static <T extends Comparable<T>> void merge(T[] array, T[] leftArray, T[] rightArray){

        int main = 0;
        int left = 0;
        int right = 0;

        while (left < leftArray.length && right < rightArray.length){
            if(leftArray[left].compareTo(rightArray[right]) < 0){
                array[main++] = leftArray[left++];
            } else{
                array[main++] = rightArray[right++];
            }
        }
        while(left < leftArray.length){
            array[main++] = leftArray[left++];
        }
        while(right < rightArray.length){
            array[main++] = rightArray[right++];
        }
    }


}
