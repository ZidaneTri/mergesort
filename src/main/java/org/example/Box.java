package org.example;

import java.util.Objects;

public class Box implements Comparable<Box> {

    private final int heigth;

    private final int width;

    private final int length;

    public int getHeigth() {
        return heigth;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getVolume(){
        return heigth * length * width;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        return heigth == box.heigth && width == box.width && length == box.length;
    }

    @Override
    public int hashCode() {
        return Objects.hash(heigth, width, length);
    }

    //Box is considered bigger than other when its volume value is bigger
    @Override
    public int compareTo(Box o) {
        return Integer.compare(this.getVolume(), o.getVolume());
    }

    public Box(int heigth, int width, int length) {
        if (heigth <= 0 || width <= 0 || length <= 0)
            throw new IllegalArgumentException();
        this.heigth = heigth;
        this.width = width;
        this.length = length;
    }

    @Override
    public String toString() {
        return "Box V=" + getVolume();
    }
}
