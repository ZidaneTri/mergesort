import org.example.Box;
import org.example.MergeSorter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class MergeSortTest {

    @Test
    public void positiveTest() {

        Box box1 = new Box(2, 6, 7);
        Box box2 = new Box(1, 2, 3);
        Box box3 = new Box(4, 6, 7);
        Box box4 = new Box(1, 2, 4);
        Box box5 = new Box(3, 5, 6);
        Box box6 = new Box(3, 4, 5);
        Box box7 = new Box(4, 5, 7);
        Box box8 = new Box(2, 3, 5);
        Box[] actual = {box1, box2, box3, box4, box5, box6, box7, box8};
        Box[] expected = {box2, box4, box8, box6, box1, box5, box7, box3};
        MergeSorter.sort(actual);
        assertArrayEquals(expected, actual);
    }

}
